const con = require("../config/db");

exports.registration = (req, res) => {
  const raceName = req.params.id;
  console.log(raceName);
  let {
    firstname,
    lastname,
    email,
    phoneNumber,
    state,
    zip,
    address,
    gender,
    age,
    city,
  } = req.body;
  console.log(req.body);

  const findQuery = `SELECT * FROM users where email='${email}';`;
  con.query(findQuery, (err, result) => {
    if (err) throw err;
    if (result.length > 0) {
      res.send({ message: "User Already Exits" });
    } else {
      const sql = `INSERT INTO users (firstname , lastname , email ,phoneNumber, state , zip , address,gender , age , city , race) VALUES ( '${firstname}' , '${lastname}' , '${email}','${phoneNumber}' , '${state}' , '${zip}' , '${address}' , '${gender}' , '${age}' , '${city}' , '${raceName}' )`;
      console.log(sql);
      con.query(sql, function (err, result) {
        if (err) throw err;
        res.send({ message: "Succesfully Registered" });
      });
    }
  });
};

exports.adminHome = (req, res) => {
  const query = `SELECT * FROM attendees;`;
  con.query(query, (err, result) => {
    if (err) throw err;
    // console.log(result);
    if (result) {
      res.send({ message: "Data Fetch SuccesfUlly", result });
    }
  });
};
exports.adminHomeAttendees = (req, res) => {
  const query = `SELECT * FROM attendees;`;
  con.query(query, (err, result) => {
    if (err) throw err;
    // console.log(result);
    if (result) {
      res.send({ message: "Data Fetch SuccesfUlly", result });
    }
  });
};

exports.adminVolunteers = (req, res) => {
  const query = `SELECT * FROM volunteer;`;
  con.query(query, (err, result) => {
    if (err) throw err;
    console.log(result);
    if (result) {
      res.send({ message: "Data Fetch SuccesfUlly", result });
    }
  });
};

exports.totalAttendees = (req, res) => {
  const query = `SELECT count(Registration_Id) AS totalAttendees FROM attendees;`;
  con.query(query, (err, result) => {
    if (err) throw err;
    console.log(result);
    if (result) {
      res.send({ message: "Data Fetch SuccesfUlly", result });
    }
  });
};

exports.totalVolunteers = (req, res) => {
  const query = `SELECT count(volunteerID) AS totalVolunteers FROM volunteer;`;
  con.query(query, (err, result) => {
    if (err) throw err;
    console.log(result);
    if (result) {
      res.send({ message: "Data Fetch SuccesfUlly", result });
    }
  });
};

exports.login = (req, res) => {
  const { email, password } = req.body;
  console.log(email, password);

  const findQuery = `SELECT *  FROM admin where email="${email}";`;
  con.query(findQuery, (err, user) => {
    if (err) throw err;
    if (user.length > 0) {
      const { email: adminEmail, password: adminPassword } = user[0];
      if (password === adminPassword) {
        const token = jwt.sign(
          {
            email: adminEmail,
          },
          JWT_SECRET,
          {
            expiresIn: "1h",
          }
        );
        res
          .status(200)
          .send({ auth: true, token, message: "Login Succesfully", user });
      } else {
        res.send({ message: "Something went wrong!!!" });
      }
    } else {
      res.send({ message: "Admin Not Found" });
    }
  });
};

exports.prizeMoney = (req, res) => {
  const { email, number, date: DOB } = req.body;
  console.log("emailnumber", req.body);

  const findQuery = `SELECT Attendee_Name FROM attendees where DOB="${DOB}" AND Contact_Number = "+91${number}" And Attendee_Email="${email}" ;`;
  con.query(findQuery, (err, result) => {
    if (err) throw err;
    if (result.length > 0) {
      res.send({ message: "Succesfully Fetched", result, status: true });
    } else {
      console.log("blaaaa");
      res.send({ message: "No User Found !!!", status: false });
    }
  });
};

exports.volunteerRegistration = (req, res) => {
  const {
    firstname,
    lastname,
    email,
    phoneNumber,
    state,
    zip,
    address,
    gender,
    age,
    city,
    emeName,
    emeNumber,
    bloodGroup,
    tShirtSize,
  } = req.body;
  console.log(req.body);

  const sql = `INSERT INTO volunteer (firstname , lastname , email ,phoneNumber, state , zip , address,gender , age , city , emergency_contact_name,emergency_contact_number ,blood_group,t_shirt_size) VALUES ( '${firstname}' , '${lastname}' , '${email}','${phoneNumber}' , '${state}' , '${zip}' , '${address}' , '${gender}' , '${age}' , '${city}' , '${emeName}','${emeNumber}','${bloodGroup}' ,'${tShirtSize}' )`;
  console.log(sql);

  const findQuery = `SELECT * FROM volunteer where email="${email}";`;
  con.query(findQuery, (err, result) => {
    if (err) throw err;
    if (result[0]) {
      res.send({ message: "User already registered", result });
    } else {
      con.query(sql, function (err, result) {
        if (err) {
          res.send({ message: "Something Went Wrong" });
          throw err;
        }
        res.send({ message: "Succesfully Registered", result });
        console.log("1 record inserted");
      });
      console.log("Succesfully Registered");
    }
  });
};

exports.km10Pdf = (req, res) => {
  console.log(__dirname);
  res.download(__dirname + "../Marathan_06.pdf");
};

exports.km5Pdf = (req, res) => {
  console.log(__dirname);
  res.download(__dirname + "../Marathan_05.pdf");
};

// exports.km5Pdf = (req, res) => {
// res.download(__dirname + "../Marathan_07.pdf");
// };
