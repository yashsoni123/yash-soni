const jwt = require("jsonwebtoken");

exports.protect = async (req, res, next) => {
  let token;

  console.log(
    "req.headers.authorization",
    req.headers.authorization.split(" ")[1]
  );
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }

  //   if (!token) {
  //     return res
  //       .status(401)
  //       .json({ message: "Not authorized to access this route" });
  //   }
  if (!token) {
    return res
      .status(401)
      .json({ message: "Not authorized to access this route" });
  } else {
    next();
  }

  //   try {
  //     const decoded = jwt.verify(token, process.env.JWT_SECRET, (err, token) => {
  //       if (err) {
  //         console.log("TOEKN VERIFY ERROR");
  //       }
  //     });
  //     console.log("decoded", decoded);

  // const user = await User.findById(decoded.id);
  // if (!user) {
  //   return next(new ErrorResponse("No User found with this id", 404));
  // }
  // req.user = user;
  next();
  //   } catch (err) {
  // return next(new ErrorResponse("Not authorized to access this route", 401));
  //     return res
  //       .status(401)
  //       .json({ message: "Not authorized to access this route please login" });
  //   }
};
