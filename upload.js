const express = require("express");
const app = express();
const bodyparser = require("body-parser");
const fs = require("fs");
const readXlsxFile = require("read-excel-file/node");
const mysql = require("mysql");
const multer = require("multer");
const path = require("path");
// import path from 'path';
var __dirname = path.resolve();
// const __dirname = path.resolve();
//use express static folder
app.use(express.static("./public"));
// body-parser middleware use
app.use(bodyparser.json());
app.use(
  bodyparser.urlencoded({
    extended: true,
  })
);
// Database connection
const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "temp",
});
// db.connect(function (err) {
// if (err) {
// return console.error('error: ' + err.message);
// }
// console.log('Connected to the MySQL server.');
// })
// Multer Upload Storage
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __dirname + "/uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
  },
});
const upload = multer({ storage: storage });
//! Routes start
//route for Home page
// -> Express Upload RestAPIs
app.post("/admin-upload", upload.single("uploadfile"), (req, res) => {
  importExcelData2MySQL(__dirname + "/uploads/" + req.file.filename);
  console.log(res);
});
// -> Import Excel Data to MySQL database
function importExcelData2MySQL(filePath) {
  // File path.
  readXlsxFile(filePath).then((rows) => {
    // `rows` is an array of rows
    // each row being an array of cells.
    console.log("rows", rows);
    // console.log("result",rows[1]);

    const data = rows[1];

    const mainData = {
      id: data[0],
      name: data[1],
      address: data[2],
      age: data[3],
    };

    console.log(mainData);

    /**
[ [ 'Id', 'Name', 'Address', 'Age' ],
[ 1, 'john Smith', 'London', 25 ],
[ 2, 'Ahman Johnson', 'New York', 26 ]
*/
    // Remove Header ROW
    rows.shift();
    // Open the MySQL connection
    db.connect((error) => {
      if (error) {
        console.error(error);
      } else {
        let query = `INSERT INTO customer (id, address, name, age) VALUES ?`;
        db.query(query, [rows], (error, response) => {
          console.log(error || response);
        });
      }
      db.end(function (err) {
        if (err) {
          return console.log(err.message);
        }
        console.log("Close All Connections");
      });
    });
  });
}
// Create a Server
let server = app.listen(8080, function () {
  let host = server.address().address;
  let port = server.address().port;
  console.log("App listening at http://%s:%s", host, port);
});
