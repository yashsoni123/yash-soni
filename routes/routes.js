const express = require("express");
const {
  registration,
  adminHome,
  adminVolunteers,
  totalAttendees,
  totalVolunteers,
  login,
  prizeMoney,
  volunteerRegistration,
  km10Pdf,
  km5Pdf,
  adminHomeAttendees,
} = require("../controllers/auth");
const { protect } = require("../middlewares/auth");
const router = express.Router();

router.route("/registration/:id").post(registration);
router.route("/admin-home").get(protect, adminHome);
router.route("/admin-home-attendees").get(adminHomeAttendees);
router.route("/admin-volunteers").get(protect, adminVolunteers);
router.route("/total-attendees").get(totalAttendees);
router.route("/total-volunteers").get(totalVolunteers);
// router.route("/login").post(login);
router.route("/prize-money").post(prizeMoney);
router.route("/volunteer/registration").post(volunteerRegistration);
router.route("/10KmPdf").get(km10Pdf);
router.route("/5KmPdf").get(km5Pdf);
router.route("/21KmPdf").get(km10Pdf);

module.exports = router;
