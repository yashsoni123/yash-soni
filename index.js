const express = require("express");
const cors = require("cors");
const bodyparser = require("body-parser");
const cookieParser = require("cookie-parser");
const fs = require("fs");
require("dotenv").config({ path: "./config.env" });
const FormData = require("form-data");
const request = require("http");
const readXlsxFile = require("read-excel-file/node");
const mysql = require("mysql");
const multer = require("multer");
const path = require("path");
const https = require("https");
var __dirname = path.resolve();
var jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("./keys");
const routes = require("./routes/routes");

const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(cors());
app.use(express.urlencoded());
app.use(
  cors({
    origin: "http://localhost:3000",
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  })
);
app.use(bodyparser.json());
app.use(
  bodyparser.urlencoded({
    extended: true,
  })
);
app.use(routes);
app.use(function (req, res, next) {
  res.header("Content-Type", "application/json;charset=UTF-8");
  res.header("Access-Control-Allow-Credentials", true);
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __dirname + "/uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
  },
});
const upload = multer({ storage: storage });

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "bhiwadi-marathon",
});

app.post("/registration/:id", (req, res) => {
  const raceName = req.params.id;
  console.log(raceName);
  let {
    firstname,
    lastname,
    email,
    phoneNumber,
    state,
    zip,
    address,
    gender,
    age,
    city,
  } = req.body;
  console.log(req.body);

  const findQuery = `SELECT * FROM users where email='${email}';`;
  con.query(findQuery, (err, result) => {
    if (err) throw err;
    if (result.length > 0) {
      res.send({ message: "User Already Exits" });
    } else {
      const sql = `INSERT INTO users (firstname , lastname , email ,phoneNumber, state , zip , address,gender , age , city , race) VALUES ( '${firstname}' , '${lastname}' , '${email}','${phoneNumber}' , '${state}' , '${zip}' , '${address}' , '${gender}' , '${age}' , '${city}' , '${raceName}' )`;
      console.log(sql);
      con.query(sql, function (err, result) {
        if (err) throw err;
        res.send({ message: "Succesfully Registered" });
      });
    }
  });
});

// app.get("/admin-home", (req, res) => {
//   const query = `SELECT * FROM attendees;`;
//   con.query(query, (err, result) => {
//     if (err) throw err;
//     console.log(result);
//     if (result) {
//       res.send({ message: "Data Fetch SuccesfUlly", result });
//     }
//   });
// });
// app.get("/admin-volunteers", (req, res) => {
//   const query = `SELECT * FROM volunteer;`;
//   con.query(query, (err, result) => {
//     if (err) throw err;
//     console.log(result);
//     if (result) {
//       res.send({ message: "Data Fetch SuccesfUlly", result });
//     }
//   });
// });
// app.get("/total-attendees", (req, res) => {
//   const query = `SELECT count(Registration_Id) AS totalAttendees FROM attendees;`;
//   con.query(query, (err, result) => {
//     if (err) throw err;
//     console.log(result);
//     if (result) {
//       res.send({ message: "Data Fetch SuccesfUlly", result });
//     }
//   });
// });
// app.get("/total-volunteers", (req, res) => {
//   const query = `SELECT count(volunteerID) AS totalVolunteers FROM volunteer;`;
//   con.query(query, (err, result) => {
//     if (err) throw err;
//     console.log(result);
//     if (result) {
//       res.send({ message: "Data Fetch SuccesfUlly", result });
//     }
//   });
// });

// let users = {
//   name: "Ritik",
//   Age: "18",
// };

app.post("/login", (req, res) => {
  const { email, password } = req.body;
  const findQuery = `SELECT *  FROM admin where email="${email}";`;
  con.query(findQuery, (err, user) => {
    if (err) throw err;
    if (user.length > 0) {
      const {
        email: adminEmail,
        password: adminPassword,
        id: adminId,
      } = user[0];
      console.log(user[0].adminId);
      if (password === adminPassword) {
        const token = jwt.sign(
          {
            id: adminId,
          },
          process.env.JWT_SECRET,
          {
            expiresIn: process.env.JWT_EXPIRES_IN,
          }
        );
        console.log("token ----->>", token);
        res
          .status(200)
          .send({ auth: true, token, message: "Login Succesfully", user });
      } else {
        res.send({ message: "Something went wrong!!!" });
      }
    } else {
      res.status(501).send({ message: "Admin Not Found" });
    }
  });
});

// app.post("/prize-money", (req, res) => {
//   const { email, number, date: DOB } = req.body;
//   console.log("emailnumber", req.body);

//   const findQuery = `SELECT Attendee_Name FROM attendees where DOB="${DOB}" AND Contact_Number = "+91${number}" And Attendee_Email="${email}" ;`;
//   con.query(findQuery, (err, result) => {
//     if (err) throw err;
//     if (result.length > 0) {
//       res.send({ message: "Succesfully Fetched", result, status: true });
//     } else {
//       console.log("blaaaa");
//       res.send({ message: "No User Found !!!", status: false });
//     }
//   });
// });

// app.post("/volunteer/registration", (req, res) => {
//   const {
//     firstname,
//     lastname,
//     email,
//     phoneNumber,
//     state,
//     zip,
//     address,
//     gender,
//     age,
//     city,
//     emeName,
//     emeNumber,
//     bloodGroup,
//     tShirtSize,
//   } = req.body;
//   console.log(req.body);

//   const sql = `INSERT INTO volunteer (firstname , lastname , email ,phoneNumber, state , zip , address,gender , age , city , emergency_contact_name,emergency_contact_number ,blood_group,t_shirt_size) VALUES ( '${firstname}' , '${lastname}' , '${email}','${phoneNumber}' , '${state}' , '${zip}' , '${address}' , '${gender}' , '${age}' , '${city}' , '${emeName}','${emeNumber}','${bloodGroup}' ,'${tShirtSize}' )`;
//   console.log(sql);

//   const findQuery = `SELECT * FROM volunteer where email="${email}";`;
//   con.query(findQuery, (err, result) => {
//     if (err) throw err;
//     if (result[0]) {
//       res.send({ message: "User already registered", result });
//     } else {
//       con.query(sql, function (err, result) {
//         if (err) {
//           res.send({ message: "Something Went Wrong" });
//           throw err;
//         }
//         res.send({ message: "Succesfully Registered", result });
//         console.log("1 record inserted");
//       });
//       console.log("Succesfully Registered");
//     }
//   });
// });

// app.get("/10KmPdf", (req, res) => {
//   res.download(__dirname + "/Marathan_06.pdf");
// });
// app.get("/5KmPdf", (req, res) => {
//   res.download(__dirname + "/Marathan_05.pdf");
// });
// app.get("/21KmPdf", (req, res) => {
//   res.download(__dirname + "/Marathan_07.pdf");
// });

app.post("/admin-upload", upload.single("uploadfile"), (req, res) => {
  importExcelData2MySQL(__dirname + "/uploads/" + req.file.filename, res);
});
function importExcelData2MySQL(filePath, res) {
  readXlsxFile(filePath).then((rows) => {
    rows.shift();

    let query = `INSERT INTO attendees (Registration_Id, Attendee_Name, Attendee_Email, Status, Ticket_Name, Order_Id, Title, DOB, Gender, Contact_Number,RACE_EVENT, Address, City, Pincode, Emergency_Contact_Name,Emergency_Contact_Number,Emergency_Contact_Relation, Blood_Group, T_SHIRT_SIZE, ADD_COUPON, Attendee_Check_In,Check_In_Time, Attendee_Badge_Print, Badge_Print_Time) VALUES  ?`;
    con.query(query, [rows], (error, response) => {
      if (error) {
        res.send({ message: error.sqlMessage, status: false });
        throw error;
      }
      if (response) {
        res.send({ message: "Succesfully Uploaded", status: true });
      }

      console.log(error || response);
    });
  });
}
app.listen(9008, () => {
  console.log("SERVER IS STARTING AT PORT NO 9008");
});
